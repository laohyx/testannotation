package com.laohyx.testannotation;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SharedMetricRegistries;

import java.util.concurrent.TimeUnit;

import static com.codahale.metrics.MetricRegistry.name;

public class Main {

    public static void main(String []argv) throws InterruptedException {
        MetricRegistry myRegistry = SharedMetricRegistries.getOrCreate("myRegistry");
        ConsoleReporter reporter = ConsoleReporter.forRegistry(myRegistry)
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();
        reporter.start(1, TimeUnit.SECONDS);

//        Timer tiemr2 = myRegistry.timer(name(com.laohyx.testannotation.Main.class, "timer2")); // timer2 works
        TimedMethod timedMethod = new TimedMethod();
        while (true) {
            timedMethod.timedMethod();
            Thread.sleep(1000);
//            try(final Timer.Context context = tiemr2.time()) {
//                Thread.sleep(300);
//            } // catch and final logic goes here

        }
    }
}

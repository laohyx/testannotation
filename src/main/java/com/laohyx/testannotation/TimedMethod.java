package com.laohyx.testannotation;

import com.codahale.metrics.annotation.Timed;
import io.astefanutti.metrics.aspectj.Metrics;

import java.util.Random;

@Metrics(registry = "myRegistry")
class TimedMethod {

    @Timed(name = "timer1")
    void timedMethod() {
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {

        }
    }
}
